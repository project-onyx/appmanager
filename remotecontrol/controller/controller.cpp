// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QDebug>

#include <unistd.h>
#include <sys/types.h>
#include <QtCore/private/qcore_unix_p.h>

#include "remoteService.h"
#include "controller.h"



bool RemoteController::sendPacket(const PacketHeader& header, const void* data) {

    if (Q_UNLIKELY(socket() == -1)) {
        return false;
    }
    if (QT_WRITE(socket(), &header, sizeof(header)) <= 0) {
        return false;
    }
    else if (::send(socket(), data, header.data_length, 0) <= 0) {
        qWarning() << "Failed to send packet";
        return false;
    }
    return true;
}


void RemoteController::registerService(ServiceId id, IRemoteService* service) {

    m_services.insert(id, service);
}


void RemoteController::readData(QSocketDescriptor socket) {
    
    PacketHeader header;
    if (QT_READ(socket, &header, sizeof(header)) != sizeof(header)) {
        qWarning() << "Failed to read packet header";
        return;
    }
    void* packet = alloca(header.data_length);

    if (::recv(socket, packet, header.data_length, 0) != header.data_length) {
        emit disconnected();
        return;
    }
    IRemoteService* service = m_services.value(header.service);
    if (service != nullptr) {
        service->handleResponse(packet, header.data_length);
    }
}


RemoteController::RemoteController(int fd, QObject* parent):
    QSocketNotifier(fd, QSocketNotifier::Read, parent)
{
    connect(this, &RemoteController::activated,
            this, &RemoteController::readData);
}
