// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef __CONTROLLER_H__
#define __CONTROLLER_H__

#include <QSocketNotifier>
#include <QHash>
#include "proto.h"


class IRemoteService;


inline uint qHash(const ServiceId& id, uint seed) noexcept {
    return qHash(static_cast<int>(id), seed);
}

// inline bool operator==(const ServiceId& lhs, const ServiceId& rhs) noexcept {
//     return static_cast<int>(lhs) == static_cast<int>(rhs);
// }

// this class implements the controller part of the remote control
class RemoteController: public QSocketNotifier {
    
    Q_OBJECT

public:
    RemoteController(int fd, QObject* parent = 0);
    virtual ~RemoteController() = default;

    bool sendPacket(const PacketHeader& header, const void* data);

    void registerService(ServiceId id, IRemoteService* service);

signals:
    void disconnected();

public slots:
    void readData(QSocketDescriptor socket);

private:
    typedef QHash<ServiceId, IRemoteService*> RemoteServices;
    RemoteServices m_services;
};


#endif // __CONTROLLER_H__