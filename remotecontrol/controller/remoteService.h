// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __REMOTESERVICE_H__
#define __REMOTESERVICE_H__

#include <QObject>
#include <proto.h>


class IRemoteService {
public:
    virtual void handleResponse(void* data, size_t size) = 0;
};


template<ServiceId ID>
class RemoteService: public IRemoteService {
protected:
    virtual void handleResponse(void* data, size_t size) override final;
    virtual void handleResponse(const Packet<ID>* packet, size_t size) = 0;
};

template<ServiceId ID>
void RemoteService<ID>::handleResponse(void* data, size_t size) {
    handleResponse(reinterpret_cast<Packet<ID>*>(data), size);
}


Q_DECLARE_INTERFACE(IRemoteService, "org.onyx.IRemoteService")


#endif // __REMOTESERVICE_H__