// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __PROTO_H__
#define __PROTO_H__

#include <cstdint>
#include "application.h"


#define MAX_SOURCE_URI_LENGTH 126
#define MAX_PACKAGE_NAME_LENGTH 256



enum class ServiceId {
    ApplicationManagement,  // This service is used to manage the application
    SystemManagement,       // This service is used to manage the system
};

struct PacketHeader {
    ServiceId service;
    uint64_t data_length;
};

template<ServiceId>
struct Packet {};

template<>
struct Packet<ServiceId::ApplicationManagement> {
    char package_id[MAX_PACKAGE_NAME_LENGTH];
    Application::CurrentStatus status;
    enum ContentType {
        ContentType_StatusReport,
        ContentType_LoggingMessage,
    } type;
};

template<>
struct Packet<ServiceId::SystemManagement> {
    enum class Command { Install, Uninstall } command;
};

struct AppReport: public Packet<ServiceId::ApplicationManagement> {
    enum Result {
        Failure = 0, Success = 1
    } result;
    size_t error_message_length = 0;
};

struct AppLogging: public Packet<ServiceId::ApplicationManagement> {
    enum class Level {
        Debug, Info, Warning, Error, Fatal
    } level;
    size_t message_length = 0;
};

struct SystemPackagePacket: public Packet<ServiceId::SystemManagement> {
    char package_id[MAX_PACKAGE_NAME_LENGTH];
};

struct InstallRequest: public SystemPackagePacket {
    char source_uri[MAX_SOURCE_URI_LENGTH];
};

struct InstallReport: public SystemPackagePacket {
    int8_t progress;
};

#endif // __PROTO_H__