# Copyright 2022 Matthieu Jacquemet
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# this class implements the guest agent
# it is a singleton
# it is used to communicate with the host
# it recieves commands from the host and forward them to the the proper service
# the service is identified by the servic field in the packet header
# this class opens a vsock connection in its constructor
class GuestAgent:

    def __init__(self):
        self.__instance = None

    def __new__(cls):
        if not GuestAgent.__instance:
            GuestAgent.__instance = object.__new__(cls)
        return GuestAgent.__instance