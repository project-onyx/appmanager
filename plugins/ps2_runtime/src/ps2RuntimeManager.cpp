// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QFile>
#include <QProcessEnvironment>
#include <QStandardPaths>

#include "ps2GameInstance.h"
#include "applicationManager.h"
#include "ps2RuntimeManager.h"



Ps2RuntimeManager::Ps2RuntimeManager(QObject* parent): NativeRuntime(parent) {
    
}


bool Ps2RuntimeManager::initialize() {

    Config* config = ApplicationManager::instance()->config();

    m_pcsx2Path = config->get<QString>( "ps2/pcsx2_path",
                                        DEFAULT_PCSX_EXECUTABLE);
    
    qWarning() << "hoho " << m_pcsx2Path << QFile::exists(m_pcsx2Path);
    if (m_pcsx2Path.isEmpty() || !QFile::exists(m_pcsx2Path)) {
        m_pcsx2Path = QStandardPaths::findExecutable("pcsx2", {
            "/usr/bin", "/usr/games", "/usr/local/games", "/usr/share/games"
        });
    }
    if (Q_UNLIKELY(m_pcsx2Path.isEmpty())) {
        qWarning() << "Could not find pcsx2 executable";
        return false;
    }
    m_gameConfigPath = config->get<QString>("ps2/game_config_path",
                                            DEFAULT_GAME_CONFIG_PATH);

    m_pcsx2Args = config->get<QStringList>( "ps2/pcsx_args",
                                            DEFAULT_PCSX_ARGS);

    m_installPath = config->get<QString>("ps2/install_path",
                                         DEFAULT_PCSX_INSTALL_PATH);

    return NativeRuntime::initialize();
}


Application* Ps2RuntimeManager::createApplication(const QVariantMap& manifest,
                                                const QString& id) {
    
    if (Q_UNLIKELY(m_applications.contains(id))) {
        return nullptr;
    }
    Ps2GameInstance* game = new Ps2GameInstance(manifest, id, this);
    return m_applications.insert(id, game).value();
}


QString Ps2RuntimeManager::pcsx2Path() const noexcept {
    return m_pcsx2Path;    
}

QStringList Ps2RuntimeManager::pcsx2Arguments() const noexcept {
    return m_pcsx2Args;
}

QString Ps2RuntimeManager::pcsx2ConfigPath() const noexcept {
    return m_gameConfigPath;
}

QString Ps2RuntimeManager::pcsx2InstallPath() const noexcept{
    return m_installPath;
}
