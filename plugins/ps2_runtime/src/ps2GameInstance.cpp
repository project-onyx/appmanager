// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QProcess>
#include <QFile>
#include <QDir>

#include "ps2RuntimeManager.h"
#include "ps2GameInstance.h"



QString checkPath(const QDir& dir, const QVariant& data, const QString& name) {
    
    if (data.isValid()) {
        return data.toString();
    }
    QString path = dir.absoluteFilePath(name);
    if (Q_LIKELY(QFile::exists(path))) {
        return path;
    }
    return QString();
}


Ps2GameInstance::Ps2GameInstance(   const QVariantMap& manifest,
                                    const QString& package,
                                    Ps2RuntimeManager* parent):
    NativeApplication(manifest, package, parent) {
    
}


Ps2GameInstance::~Ps2GameInstance() {
    
}


bool Ps2GameInstance::initialize() {

    QVariant isop = m_manifest["X-PlayStation2/IsoPath"];
    QVariant cfgf = m_manifest["X-PlayStation2/ConfigFile"];
    QVariant cfgp = m_manifest["X-PlayStation2/ConfigPath"];

    const QDir insDir(manager()->pcsx2InstallPath());
    const QDir cfgDir(manager()->pcsx2ConfigPath() + "/" + m_package);

    QString configFile = checkPath(cfgDir, cfgf, "config.ini");
    QString configPath = checkPath(cfgDir, cfgp, "inis");
    QString isoFileDir = checkPath(insDir, isop, m_package + ".iso");

    QStringList arguments = manager()->pcsx2Arguments();

    if (Q_UNLIKELY(isoFileDir.isNull())) {
        return report("Could not find ISO file");
    } else if (!configFile.isNull()) {
        arguments << "--cfg=" + configFile;
    } else if (!configPath.isNull()) {
        arguments << "--cfgpath=" + configPath;
    }
    arguments << isoFileDir;

    m_process->setProgram(manager()->pcsx2Path());
    m_process->setArguments(arguments);

    return true;
}


QProcessEnvironment Ps2GameInstance::setupEnvironment() {
    
    QProcessEnvironment environ = NativeApplication::setupEnvironment();
    environ.insert("XDG_SESSION_TYPE", "x11");
    environ.insert("GDK_BACKEND", "x11"); // pcsx2 is not compatible with wayland

    return environ;
}


Ps2RuntimeManager* Ps2GameInstance::manager() const noexcept {
    return static_cast<Ps2RuntimeManager*>(parent());
}
