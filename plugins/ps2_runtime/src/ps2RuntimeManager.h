// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __PS2RUNTIME_H__
#define __PS2RUNTIME_H__

#include <QObject>
#include <QMap>
#include "runtimeManager.h"
#include "nativeRuntime.h"

class Ps2GameInstance;


class Ps2RuntimeManager: public NativeRuntime {

    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.onyx.IRuntimeManager" FILE "metadata.json")
    Q_INTERFACES(IRuntimeManager)

public:
    Ps2RuntimeManager(QObject* parent = nullptr);

    virtual bool initialize() override;
    virtual Application* createApplication( const QVariantMap& manifest,
                                            const QString& id) override;
    
    QString pcsx2Path() const noexcept;
    QStringList pcsx2Arguments() const noexcept;
    QString pcsx2ConfigPath() const noexcept;
    QString pcsx2InstallPath() const noexcept;

private:
    QStringList m_pcsx2Args;
    QString m_pcsx2Path, m_gameConfigPath, m_installPath;
};


#endif // __PS2RUNTIME_H__