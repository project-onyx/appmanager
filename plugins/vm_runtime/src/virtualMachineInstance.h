// Copyright 2022 thomas
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef __VIRTUALMACHINEINSTANCE_H__
#define __VIRTUALMACHINEINSTANCE_H__

#include <QObject>
#include <QLoggingCategory>

#include <libvirt/libvirt.h>


Q_DECLARE_LOGGING_CATEGORY(VmInstance)

class VirtualMachineInstance: public QObject {

    Q_OBJECT

public:
    VirtualMachineInstance(const QString& uri, QObject* parent=nullptr);
    virtual ~VirtualMachineInstance();

    bool create(const QString& name, int numPciDevices);
    bool isGuestRunning() const;
    bool startGuest();
    bool stopGuest();
    qint64 getGuestProcessId() const;

signals:
    void guestWasShutDown();

protected:
    virConnectPtr m_connection = nullptr;
    virDomainPtr m_domain = nullptr;
    mutable qint64 m_guestPid = -1;
};

#endif // __VIRTUALMACHINEINSTANCE_H__