// Copyright 2022 thomas
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QFile>
#include <common_utils.h>
#include <QSocketNotifier>
#include <QTimer>

#include <libvirt/virterror.h>

#include "applicationManager.h"
#include "virtualMachineInstance.h"


static QVector<QSocketNotifier*> events;
static QVector<QTimer*> timeouts;
static bool registered = false;


Q_LOGGING_CATEGORY(VmInstance, "VmInstance")


static QSocketNotifier::Type virEventToQt(int event) {

    if (event & VIR_EVENT_HANDLE_READABLE) {
        return QSocketNotifier::Read;
    } else if (event & VIR_EVENT_HANDLE_WRITABLE) {
        return QSocketNotifier::Write;
    }
    Q_ASSERT_X(false, "LibVirtEvent::virToQt", "Unknown event type");
}

static int qtEventToVir(QSocketNotifier::Type type) {

    if (type == QSocketNotifier::Read) {
        return VIR_EVENT_HANDLE_READABLE;
    } else if (type == QSocketNotifier::Write) {
        return VIR_EVENT_HANDLE_WRITABLE;
    }
    Q_ASSERT_X(false, "LibVirtEvent::qtToVir", "Unknown event type");
}

static QtMsgType virErrorLevelToQt(virErrorLevel level) {
    
    switch (level) {
        case VIR_ERR_NONE: return QtDebugMsg;
        case VIR_ERR_WARNING: return QtWarningMsg;
        case VIR_ERR_ERROR: return QtCriticalMsg;
    }
    return QtCriticalMsg;
}   

static int registerHandle( int fd, int event, virEventHandleCallback cb,
                            void *opaque, virFreeCallback ff) {

    auto* notifier = new QSocketNotifier(fd, virEventToQt(event));
    
    int watch = events.indexOf(nullptr);
    if (watch == -1) {
        watch = events.size();
        events.append(notifier);
    } else {
        events[watch] = notifier;
    }
    QObject::connect(notifier, &QSocketNotifier::activated,
                    [=](QSocketDescriptor, QSocketNotifier::Type type) {

        (cb)(watch, fd, qtEventToVir(type), opaque);
    });
    return watch;
}


static void updateHandle(int watch, int event) {

    QSocketNotifier* notifier = events.at(watch);
    if (notifier == nullptr) {
        return;
    }
    if (!event && notifier->isEnabled()) {
        notifier->setEnabled(false);
    } else if (event && !notifier->isEnabled()) {
        notifier->setEnabled(true);
    }
}

static int unregisterHandle(int watch) {
    
    QSocketNotifier* notifier = events.at(watch);
    if (notifier == nullptr) {
        return -1;
    }
    notifier->setEnabled(false);
    delete notifier;
    events[watch] = nullptr;
    return 0;
}


static int addTimeout(int timeout, virEventTimeoutCallback cb,
                        void* opaque, virFreeCallback f) {
    
    QTimer* timer = new QTimer();
    timer->setInterval(timeout);

    int index = timeouts.indexOf(nullptr);
    if (index == -1) {
        index = timeouts.size();
        timeouts.append(timer);
    } else {
        timeouts[index] = timer;
    }
    QObject::connect(timer, &QTimer::timeout, std::bind(cb, index, opaque));

    return index;
}


static void updateTimeout(int index, int timeout) {

    QTimer* timer = timeouts.at(index);
    if (timer != nullptr) {
        timer->setInterval(timeout);
    }
}


static int removeTimeout(int index) {

    QTimer* timer = timeouts.at(index);
    if (timer == nullptr) {
        return -1;
    }
    timer->stop();
    delete timer;
    timeouts[index] = nullptr;
    return 0;
}


// replace every occurance of ${VAR} with the value with key VAR in the application
// configuration under the section vm_config
static QString substituteConfig(const QString& source) {

    Config* config = ApplicationManager::instance()->config();
    QString result = source;

    for (const QString& key : config->keys("vm_config/*")) {
        result.replace("${" + key.mid(10) + "}", config->get<QString>(key));
    }
    return result;
}


void repportError(void* userData, virErrorPtr error) {
    Q_UNUSED(userData)

    QtMsgType type = virErrorLevelToQt(error->level);
    QString errorFmt("%1: %2: %3");

    if (!VmInstance().isEnabled(type)) {
        return;
    }
    QDebug(type) << qPrintable(errorFmt
        .arg(QString::fromUtf8(VmInstance().categoryName()))
        .arg(QString::number(error->code))
        .arg(QString::fromUtf8(error->message))
    );
}


INITIALIZER(libvirt) {

    virEventRegisterImpl(registerHandle, updateHandle,
                        unregisterHandle, addTimeout,
                        updateTimeout, removeTimeout);

    virSetErrorFunc(nullptr, repportError);
}




VirtualMachineInstance::VirtualMachineInstance(const QString& uri, QObject* parent):
    QObject(parent),
    m_connection(virConnectOpen(C_STR(uri))) {

}

VirtualMachineInstance::~VirtualMachineInstance() {

    if (m_domain != nullptr) {
        virDomainDestroy(m_domain);
        virDomainFree(m_domain);
    }
    if (m_connection != nullptr) {
        virConnectClose(m_connection);
    }
}

bool VirtualMachineInstance::isGuestRunning() const {
    if (Q_UNLIKELY(m_domain == nullptr)) {
        return false;
    }
    return virDomainIsActive(m_domain) == 1;
}

bool VirtualMachineInstance::startGuest() {
    if (Q_UNLIKELY(isGuestRunning())) {
        return false;
    } else if (virDomainCreate(m_domain) == 0) {
        return true;
    }
    return false;
}

bool VirtualMachineInstance::stopGuest() {
    if (Q_UNLIKELY(!isGuestRunning())) {
        return false;
    } else if (virDomainDestroy(m_domain) == 0) {
        return true;
    }
    return false;
}


bool VirtualMachineInstance::create(const QString& file, int numPciDevices) {

    if (Q_UNLIKELY(m_domain != nullptr)) {
        qCWarning(VmInstance) << "Another domain was already created";
        return false;
    }
    QFile domFile(file);
    if (!domFile.open(QIODevice::ReadOnly)) {
        qCWarning(VmInstance) << "Open domain file: " << domFile.errorString();
        return false;
    }
    QString xmlContent = substituteConfig(domFile.readAll());
    domFile.close();

    if (!(m_domain = virDomainCreateXML(m_connection, C_STR(xmlContent), 0))) {
        return false;
    } else {
        virConnectDomainEventRegisterAny(m_connection, m_domain,
                                    VIR_DOMAIN_EVENT_ID_LIFECYCLE,
                                    [](virConnectPtr, virDomainPtr, void* opaque) {
            auto* vm = static_cast<VirtualMachineInstance*>(opaque);
            vm->m_guestPid = -1;
            emit vm->guestWasShutDown();
        }, this, nullptr);
    }
    return true;
}


qint64 VirtualMachineInstance::getGuestProcessId() const {

    if (Q_LIKELY(!isGuestRunning() || m_guestPid != -1)) {
        return m_guestPid;
    }
    QRegularExpression expr("^(.+)://");

    auto match = expr.match(virConnectGetURI(m_connection));
    if (Q_UNLIKELY(!match.hasMatch())) {
        qCritical() << "Failed to parse the libvirt URI";
        return -1;
    }
    QFile pidFile(QString("/run/libvirt/%1/%2.pid")
        .arg(match.captured(1))
        .arg(virDomainGetName(m_domain)));

    if (pidFile.open(QIODevice::ReadOnly)) {
        m_guestPid = pidFile.readAll().toLongLong();
    }
    return m_guestPid;
}


    // for (int index=0; index<numPciDevices; ++index) {
    //     virDomainAttachDevice(m_domain, C_STR(QString(
    //         "<controller type='pci' index='%1' model='pcie-root-port'>\n"
    //             "<model name='pcie-root-port'/>\n"
    //             "<target chassis='%1' port='0x%2'/>\n"
    //             "<address type='pci' domain='0x0000' bus='0x00' slot='0x01' "
    //                 "function='0x%3' %4/>"
    //         "</controller>")
    //             .arg(index + 1)
    //             .arg(index + 8, 2, 16, QChar('0'))
    //             .arg(index, 2, 16, QChar('0'))
    //             .arg(index == 0 ? "multifunction='yes'" : ""))
    //     );
    // }