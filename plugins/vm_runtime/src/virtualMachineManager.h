// Copyright 2022 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __PLUGIN_H__
#define __PLUGIN_H__

#include <QSocketNotifier>
#include <QHash>

#include "controller.h"
#include "remoteService.h"
#include "runtimeManager.h"


class VirtualMachineApplication;
class RemoteController;
class PacketHeader;
class QProcess;
class VirtualMachineInstance;


typedef RemoteService<ServiceId::ApplicationManagement> ApplicationService;
typedef Packet<ServiceId::ApplicationManagement> ApplicationPacket;


class VirtualMachineManager:    public QSocketNotifier,
                                public ApplicationService,
                                public IRuntimeManager {

    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.onyx.IRuntimeManager" FILE "metadata.json")
    Q_INTERFACES(IRuntimeManager)
    Q_INTERFACES(IRemoteService)

public:
    VirtualMachineManager();
    virtual ~VirtualMachineManager();

    bool initialize() override;
    Application* createApplication( const QVariantMap& manifest,
                                    const QString& id) override;
    
    void handleResponse(const ApplicationPacket* packet, size_t size) override;

    template<ServiceId ID>
    bool sendRequest(const Packet<ID>& packet);

    bool startLookingGlass(VirtualMachineApplication* app);
    bool stopLookingGlass();
    bool setCurrent(VirtualMachineApplication* application);
    VirtualMachineInstance* vmInstance() const noexcept;
    QProcess* lookingGlassProcess() const noexcept;

private slots:
    void disconnection();
    void handleInstanceDestroyed();

protected:
    void setupController(int fd);
    static int setupServer();

    typedef QHash<QString, VirtualMachineApplication*> Applications;
    Applications m_applications;

    QProcess* m_lookingGlass;
    VirtualMachineInstance* m_instance = nullptr;
    RemoteController* m_controller = nullptr;
    VirtualMachineApplication* m_current = nullptr;
};



template<ServiceId ID>
bool VirtualMachineManager::sendRequest(const Packet<ID>& packet) {
    
    if (Q_UNLIKELY(m_controller == nullptr)) {
        return false;
    }
    PacketHeader header {
        .service = ID,
        .data_length = sizeof(packet)
    };
    return m_controller->sendPacket(header, &packet);
}


#endif // __PLUGIN_H__