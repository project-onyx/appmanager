// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __VIRTUALMACHINEAPPLICATION_H__
#define __VIRTUALMACHINEAPPLICATION_H__

#include "application.h"


class VirtualMachineManager;
class ApplicationReport;

class AppReport;
class AppLogging;


// This is the the implementation of Application interface
// It is used to instantiate an application inside the virtual machine
// the runtime manager is passed to the constructor parameter
class VirtualMachineApplication: public Application {

    Q_OBJECT

public:
    VirtualMachineApplication(  const QVariantMap& manifest,
                                const QString& id,
                                VirtualMachineManager* mgr);

    virtual ~VirtualMachineApplication() = default;

    virtual bool initialize() override;
    virtual qint64 instanceId() const override;
    virtual void processRequest(CurrentStatus status) override;
    virtual bool enableInputs(bool enabled) override;

    void handleStatusReport(const AppReport* data, size_t size);
    void handleLoggingMessage(const AppLogging* data, size_t size);

private:
    int16_t m_id = -1;
    VirtualMachineManager* manager() const;
};


#endif // __VIRTUALMACHINEAPPLICATION_H__