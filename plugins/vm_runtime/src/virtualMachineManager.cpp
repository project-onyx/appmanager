// Copyright 2022 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <sys/socket.h>
#include <linux/vm_sockets.h>
#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <linux/net.h>
#include <linux/netlink.h>
#include "applicationManager.h"
#include "virtualMachineApplication.h"

#include <common_utils.h>

#include <QProcess>
#include <QStandardPaths>
#include <QDir>
#include <QTcpSocket>
#include <QMetaEnum>

#include "virtualMachineInstance.h"
#include "proto.h"
#include "virtualMachineManager.h"


static QString libVirtUri() {

    Config* config = ApplicationManager::instance()->config();
    return config->get<QString>("runtime:vm/libvirt_uri",
                                DEFAULT_LIBVIRT_URI);
}


VirtualMachineManager::VirtualMachineManager():
    QSocketNotifier(setupServer(), QSocketNotifier::Read),
    m_lookingGlass(new QProcess(this)),
    m_instance(new VirtualMachineInstance(libVirtUri(), this)) {

    // find looking-glass executable
    QString exec = QStandardPaths::findExecutable("looking-glass-client");
    if (exec.isEmpty()) {
        qCritical() << "looking-glass-client not found";
    } else {
        m_lookingGlass->setProgram(exec);
    }
    auto lgEnd = QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished);

    QObject::connect(m_lookingGlass, lgEnd, [this](int retCode) {
        m_current = nullptr;
    });

    connect(this, &QSocketNotifier::activated, [this]() {
        Q_ASSERT(m_controller == nullptr);

        struct sockaddr_vm addr;
        socklen_t addrlen = sizeof(addr);
        
        int fd = accept(socket(), (struct sockaddr*)&addr, &addrlen);
        setupController(fd);
    });

    connect(m_instance, &VirtualMachineInstance::guestWasShutDown,
            this, &VirtualMachineManager::handleInstanceDestroyed);
}


VirtualMachineManager::~VirtualMachineManager() {
    m_lookingGlass->terminate();
}


void VirtualMachineManager::handleResponse( const ApplicationPacket* packet,
                                            size_t size) {


    auto* app = m_applications.value(packet->package_id);
    if (app == nullptr) {
        qWarning() << "Application not found" << packet->package_id;
        return;
    }
    switch (packet->type) {
        case ApplicationPacket::ContentType_StatusReport:
            return app->handleStatusReport((const AppReport*)packet, size);
        case ApplicationPacket::ContentType_LoggingMessage:
            return app->handleLoggingMessage((const AppLogging*)packet, size);
        default:
            qWarning() << "Unhandled packet type" << packet->type;
            break;
    }
}


bool VirtualMachineManager::setCurrent(VirtualMachineApplication* application) {

    if (m_current == nullptr || application == nullptr) {
        m_current = application;
        return true;
    }
    return false;
}

VirtualMachineInstance* VirtualMachineManager::vmInstance() const noexcept {
    return m_instance;
}

QProcess* VirtualMachineManager::lookingGlassProcess() const noexcept {
    return m_lookingGlass;
}

void VirtualMachineManager::disconnection() {

    m_controller->deleteLater();
    m_controller = nullptr;
}

void VirtualMachineManager::handleInstanceDestroyed() {

    for (VirtualMachineApplication* app: m_applications) {
        app->Application::requestStatus(CurrentStatus::Stopped);
    }    
}


void VirtualMachineManager::setupController(int fd) {
    Q_ASSERT(m_controller == nullptr);

    m_controller = new RemoteController(fd, this);
    m_controller->registerService(ServiceId::ApplicationManagement, this);

    connect(m_controller, &RemoteController::disconnected,
            this, &VirtualMachineManager::disconnection);
}


bool VirtualMachineManager::startLookingGlass(VirtualMachineApplication* app) {
    Q_ASSERT(m_lookingGlass->state() == QProcess::NotRunning);

    QVariantMap manifest = app->manifest();
    QProcessEnvironment environ = QProcessEnvironment::systemEnvironment();
    environ.insert("APPLICATION_ID", app->package());

    m_lookingGlass->setProcessEnvironment(environ);
    m_lookingGlass->setArguments({
        "win:fullscreen=true",
        "win:title=" + manifest["name"].toString(),
        "win:icon=" + manifest["icon"].toString(),
    });
    m_lookingGlass->start();

    if (!m_lookingGlass->waitForStarted(5000)) {
        qCritical() << "Failed to start looking-glass-client";
        return false;
    }
    return true;
}

bool VirtualMachineManager::stopLookingGlass() {
    Q_ASSERT(m_lookingGlass->state() == QProcess::Running);

    m_lookingGlass->terminate();

    if (!m_lookingGlass->waitForFinished(5000)) {
        qCritical() << "Failed to stop looking-glass-client";
        return false;
    }
    return true;
}


int VirtualMachineManager::setupServer() {
    // this function initialize a vsock server for the virtual machine
    // as soon as the gest connects to this socket, then isVmReady() will return true
    // and the application can be created
    Config* config = ApplicationManager::instance()->config();

    int socket_fd = ::socket(AF_VSOCK, SOCK_STREAM, 0);

    if (Q_UNLIKELY(socket_fd < 0)) {
        perror("Virtual Machine server creation failed");
        return -1;
    }
    struct sockaddr_vm addr = {
        .svm_family = AF_VSOCK,
        .svm_port = DEFAULT_SERVER_PORT,
        .svm_cid = VMADDR_CID_ANY,
	};
    addr.svm_port = config->get<int>("runtime:vm/server_port", addr.svm_port);


    if (::bind(socket_fd, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
        perror("Virtual Machine server bind failed");
        return -1;
    }
    if (::listen(socket_fd, 1) < 0) {
        perror("Virtual Machine server listen failed");
        return -1;
    }
    return socket_fd;
}


bool VirtualMachineManager::initialize() {
    return false;
    Config* config = ApplicationManager::instance()->config();

    QString domain = config->get<QString>(
                            "runtime:vm/libvirt_domain",
                            DEFAULT_LIBVIRT_DOMAIN);

    int numPciSlots = config->get<int>("runtime:vm/pci_slots", 8);
    
    if (!QFile::exists(domain)) {
        QDir dir(config->get<QString>(  "runtime:vm/libvirt_domain_dir",
                                        DEFAULT_LIBVIRT_DOMAIN_DIR));
        QString fullpath = dir.absoluteFilePath(domain + ".xml");
        if (QFile::exists(fullpath)) {
            domain = qMove(fullpath);
        } else {
            qWarning() << "Domain do not exists:" << domain;
            return false;
        }
    }
    return m_instance->create(domain, numPciSlots);
}


Application* VirtualMachineManager::createApplication(  const QVariantMap& manifest,
                                                        const QString& id) {
    
    if (Q_UNLIKELY(m_applications.contains(id))) {
        return nullptr;
    }
    auto* instance = new VirtualMachineApplication(manifest, id, this);
    return m_applications.insert(id, instance).value();
}