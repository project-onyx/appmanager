// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QtDBus/QDBusMessage>
#include <QProcess>

#include "proto.h"
#include "virtualMachineManager.h"
#include <common_utils.h>
#include "virtualMachineInstance.h"
#include "virtualMachineApplication.h"


static QtMsgType convertMsgType(AppLogging::Level level) {

    switch (level) {
        case AppLogging::Level::Debug: return QtDebugMsg;
        case AppLogging::Level::Info: return QtInfoMsg;
        case AppLogging::Level::Warning: return QtWarningMsg;
        case AppLogging::Level::Error: return QtCriticalMsg;
        case AppLogging::Level::Fatal: return QtFatalMsg;
    }
    Q_UNREACHABLE();
    return QtDebugMsg;
}


VirtualMachineApplication::VirtualMachineApplication(const QVariantMap& manifest,
                                                    const QString& package,
                                                    VirtualMachineManager* mgr):
    Application(manifest, package, mgr)
{
   
}

bool VirtualMachineApplication::initialize() {
    // nothing to do
    return true;
}


void VirtualMachineApplication::processRequest(CurrentStatus status) {

    if (status == CurrentStatus::Running) {
        if (!manager()->setCurrent(this)) {
            return responseReady(status, false, tr("Another application "
                                                   "is already running"));
        }
    } else if (status == CurrentStatus::Stopped) {
        manager()->setCurrent(nullptr);
    }
    Packet<ServiceId::ApplicationManagement> packet {
        .status = status,
    };
    strcpy(packet.package_id, C_STR(m_package));
    if (Q_UNLIKELY(!manager()->sendRequest(packet))) {
        return responseReady(status, false, "Failed to send the request "
                                            "to the virtual machine");
    }
}


bool VirtualMachineApplication::enableInputs(bool enabled) {
    if (m_status == CurrentStatus::Stopped) {
        return false;
    }
    qint64 pid = manager()->vmInstance()->getGuestProcessId();
    return grabInputsForProcess(enabled ? pid : -1);
}


void VirtualMachineApplication::handleStatusReport( const AppReport* data,
                                                    size_t size) {

    if (Q_UNLIKELY(data->status == Unknown)) {
        return;
    }
    if (data->result && data->status == Application::Running) {
        manager()->startLookingGlass(this);
    } 
    else if (data->result && data->status == Application::Stopped) {
        manager()->stopLookingGlass();
    }
    QString error;
    if (!data->result && data->error_message_length > 0) {
        const char* buffer = reinterpret_cast<const char*>(data + 1);
        size_t bufferSize = size - sizeof(AppReport);

        if (data->error_message_length > bufferSize) {
            report("Invalid error message length", QtWarningMsg);
        } else {
            bufferSize = data->error_message_length;
        }
        error = QString::fromUtf8(buffer, bufferSize);
    }
    Application::responseReady(data->status, data->result, error);
}


void VirtualMachineApplication::handleLoggingMessage(const AppLogging* data,
                                                     size_t size) {

    const char* buffer = reinterpret_cast<const char*>(data + 1);
    size_t bufferSize = size - sizeof(AppLogging);

    if (data->message_length > bufferSize) {
        report("Invalid error message length", QtWarningMsg);
    } else {
        bufferSize = data->message_length;
    }
    report(QString::fromUtf8(buffer, bufferSize), convertMsgType(data->level));
}


qint64 VirtualMachineApplication::instanceId() const {
    return manager()->lookingGlassProcess()->processId();
}


VirtualMachineManager* VirtualMachineApplication::manager() const {
    return static_cast<VirtualMachineManager*>(parent());
}
