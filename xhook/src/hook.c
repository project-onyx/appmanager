// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#define _GNU_SOURCE

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <string.h>
#include <xcb/xcb.h>

#include <stdlib.h>
#include <dlfcn.h>
#include <macro_utils.h>


NOEXPORT const char* _applicationId;

INITIALIZER(applicationId) {
    _applicationId = getenv("XDG_APPLICATION_ID");
}


typedef xcb_void_cookie_t (*xcb_change_property_t)(xcb_connection_t *conn,
                                uint8_t mode, xcb_window_t window,
                                xcb_atom_t property, xcb_atom_t type,
                                uint8_t format, uint32_t data_len,
                                const void *data);


typedef int (*XSetClassHint_t)(Display*, Window, XClassHint*);

typedef void (*XSetWMProperties_t)(Display*, Window, XTextProperty*,
                                XTextProperty*, char**, int, XSizeHints*,
                                XWMHints*, XClassHint*);



EXPORT int XSetClassHint(Display* display, Window w, XClassHint* class_hint) {

    static XSetClassHint_t realCall= 0;
    if (UNLIKELY(!realCall)) {
        realCall = (XSetClassHint_t)dlsym(RTLD_NEXT, "XSetClassHint");
    }
    if (_applicationId != NULL) {
        class_hint->res_class = strdup(_applicationId);
    }
    return realCall(display, w, class_hint);
}



EXPORT void XSetWMProperties(Display* display, Window w,
                            XTextProperty *window_name, XTextProperty *icon_name,
                            char **argv, int argc, XSizeHints *normal_hints,
                            XWMHints *wm_hints, XClassHint *class_hints) {

    static XSetWMProperties_t realCall= 0;
    if (UNLIKELY(!realCall)) {
        realCall = (XSetWMProperties_t)dlsym(RTLD_NEXT, "XSetWMProperties");
    }
}


EXPORT xcb_void_cookie_t xcb_change_property(xcb_connection_t *conn, uint8_t mode,
                                            xcb_window_t window, xcb_atom_t property,
                                            xcb_atom_t type, uint8_t format,
                                            uint32_t data_len, const void *data) {

    static xcb_change_property_t realCall= 0;
    if (UNLIKELY(!realCall)) { 
        realCall = (xcb_change_property_t)dlsym(RTLD_NEXT, "xcb_change_property");
    }
    if (property == XCB_ATOM_WM_CLASS && _applicationId != NULL) {
        data = _applicationId;
        data_len = strlen(_applicationId);
    }
    return realCall(conn, mode, window, property, type, format, data_len, data);
}