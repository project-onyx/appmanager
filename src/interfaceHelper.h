// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __INTERFACEHELPER_H__
#define __INTERFACEHELPER_H__

#include <QtDBus/qdbusmetatype.h>
#include <QMap>
#include <QString>
#include <QObject>
#include "applicationStatus.h"
#include <QDebug>

#include "common_macro_utils.h"
#include <common_utils.h>


typedef ApplicationStatus::CurrentStatus CurrentStatus;
typedef QMap<QString, QDBusVariant> DBusVariantMap;
typedef QMap<QString, QDBusObjectPath> QDBusObjectPathMap;

Q_DECLARE_METATYPE(CurrentStatus)
Q_DECLARE_METATYPE(DBusVariantMap)
Q_DECLARE_METATYPE(QDBusObjectPathMap)


inline const QDBusArgument& operator>>( const QDBusArgument& argument,
                                        CurrentStatus& status) {

    int statusInt;
    argument.beginStructure();
    argument >> statusInt;
    argument.endStructure();

    if (Q_UNLIKELY(statusInt < 0 || statusInt > CurrentStatus::Stopped)) {
        status = CurrentStatus::Unknown;
    } else {
        status = static_cast<CurrentStatus>(statusInt);
    }
    return argument;
}


inline QDBusArgument& operator<<(   QDBusArgument& argument,
                                    const CurrentStatus& status) {

    argument.beginStructure();
    argument << int(status);
    argument.endStructure();

    return argument;
}

inline QDataStream& operator<<(QDataStream& stream, const CurrentStatus& status) {
    stream << quint8(status);
    return stream;
}

inline QDataStream& operator>>(QDataStream& stream, CurrentStatus& status) {
    quint8 statusInt;
    stream >> statusInt;
    if (Q_UNLIKELY(statusInt > CurrentStatus::Stopped)) {
        status = CurrentStatus::Unknown;
    } else {
        status = static_cast<CurrentStatus>(statusInt);
    }
    return stream;
}

INITIALIZER(initInterfaceHelper) {

    qDBusRegisterMetaType<DBusVariantMap>();
    qDBusRegisterMetaType<QDBusObjectPathMap>();
    qDBusRegisterMetaType<CurrentStatus>();
}



#endif // __INTERFACEHELPER_H__