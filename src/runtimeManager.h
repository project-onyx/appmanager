// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef __RUNTIMEMANAGER_H__
#define __RUNTIMEMANAGER_H__

#include <QObject>

class Application;

class IRuntimeManager {

public:
    virtual ~IRuntimeManager() = default;

    virtual bool initialize() = 0;
    virtual Application* createApplication( const QVariantMap& manifest,
                                            const QString& id) = 0;
protected:
    IRuntimeManager() {}
};

Q_DECLARE_INTERFACE(IRuntimeManager, "org.onyx.IRuntimeManager")

#endif // __RUNTIMEMANAGER_H__