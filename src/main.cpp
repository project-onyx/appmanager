// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QSettings>
#include <QLockFile>
#include <QCommandLineParser>

#include <unistd.h>
#include <sys/types.h>
#include <csignal>

#include "app_config.h"
#include "applicationManager.h"
#include <QFile>

int main(int argc, char *argv[]) {

    // Check if there is no other instance running using lock file
    QLockFile lockFile(QStringLiteral("/var/run/user/%1/%2.pid").arg(getuid())
                                                                .arg(APP_NAME));
    if (!lockFile.tryLock(1000)) {
        qWarning() << "Another instance is already running";
        return EXIT_FAILURE;
    }
    std::signal(SIGINT,  [](int) { QCoreApplication::quit(); });
    std::signal(SIGTERM, [](int) { QCoreApplication::quit(); });

    ApplicationManager::setApplicationName(APP_NAME);
    ApplicationManager::setOrganizationName(ORG_NAME);
    ApplicationManager::setApplicationVersion(APP_VERSION);

    QCommandLineParser parser;
    parser.setApplicationDescription(
        "This program is a daemon that manages the applications."
    );
    parser.addHelpOption();
    parser.addVersionOption();

    parser.addOption(QCommandLineOption(
        "config", "Use the specified configuration file", "file"
        DEFAULT_CONFIG_PATH
    ));

    ApplicationManager application(argc, argv);
    parser.process(application);

    application.setupCommand(parser);

    return application.exec();
}