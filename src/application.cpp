// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QMessageLogger>
#include <QDBusConnection>

#include "inputmanagerinterface.h"

#include "applicationManager.h"
#include "applicationadaptor.h"
#include "application.h"

Q_LOGGING_CATEGORY(App, "app")


Application::Application(   const QVariantMap& manifest,
                            const QString& package,
                            QObject* parent):
    ApplicationStatus(parent),
    m_manifest(manifest),
    m_package(package),
    m_status(CurrentStatus::Stopped)
{

    QDBusConnection::sessionBus().registerObject(
        objectPath(), this,
        QDBusConnection::ExportScriptableContents |
        QDBusConnection::ExportAllInvokables |
        QDBusConnection::ExportAllProperties
    );
}

Application::~Application() {
    
    QDBusConnection::sessionBus().unregisterObject(
        objectPath(), QDBusConnection::UnregisterNode
    );
}

void Application::requestStatus(qint32 status) {

    auto current_status = CurrentStatus(status);

    const QDBusMessage& mesg = QDBusContext::message();
    mesg.setDelayedReply(true);

    if (current_status == m_status || current_status == CurrentStatus::Unknown) {
        QDBusConnection::sessionBus().send(
            mesg.createErrorReply(  QDBusError::InvalidArgs,
                                    tr("Invalid status")));
    } else if (isResponsePending(current_status)) {
        QDBusConnection::sessionBus().send(
            mesg.createErrorReply(  QDBusError::InvalidArgs,
                                    tr("Status already requested")));
    } else {
        QDBusMessage& reply = m_pendingReplies[status];
        reply = mesg.createReply();
        if (Q_LIKELY(QDBusConnection::sessionBus().send(reply))) {
            processRequest(current_status);
        }
    }
}

bool Application::enableInputs(bool enabled) {
    if (Q_UNLIKELY(m_status == CurrentStatus::Stopped)) {
        return false;
    }
    return grabInputsForProcess(enabled ? instanceId() : -1);    
}


QString Application::package() const noexcept {
    return m_package;
}

Application::CurrentStatus Application::status() const noexcept {
    return m_status;
}

QVariantMap Application::manifest() const noexcept {
    return m_manifest;    
}

QString Application::objectPath() const noexcept {
    return "/" + QString(m_package).replace('.', '/');
}


bool Application::report(const QString& message, QtMsgType type) {

    if (App().isEnabled(type)) {
        QString fmt("[%1]: %2");
        QDebug(type) << qPrintable(fmt.arg(m_package).arg(message));
    }
    if (type == QtFatalMsg || type == QtCriticalMsg) {
        emit qApp->applicationError(m_package, message);
    }
    return false;
}


bool Application::grabInputsForProcess(qint64 pid) {

    static OrgOnyxInputManagerInterface manager("org.onyx.InputManager",
                                                "/InputManager",
                                                QDBusConnection::systemBus());

    if (Q_UNLIKELY(!manager.isValid())) {
        qCCritical(App) << "Failed to connect to InputManager";
        return false;
    }
    auto reply = manager.setFocusedProcess(pid);
    reply.waitForFinished();

    if (Q_UNLIKELY(!reply.isValid())) {
        qCCritical(App) << "Failed to set focused process";
        return false;
    }
    return true;
}


bool Application::isResponsePending(CurrentStatus status) const noexcept {
    return m_pendingReplies[status].type() != QDBusMessage::InvalidMessage;
}


bool Application::setStatus(CurrentStatus status) {
    
    if (Q_LIKELY(m_status != status)) {
        m_status = status;
        emit statusChanged(status);
        return true;
    }
    return false;
}

void Application::responseReady(CurrentStatus status, bool success,
                                const QString& error) {
    
    if (Q_UNLIKELY(!isResponsePending(status))) {
        return void(success && setStatus(status));
    }
    QDBusMessage& reply = m_pendingReplies[status];
    reply.setArguments(QVariantList() << success);
    if (!success) {
        reply = reply.createErrorReply(QDBusError::InternalError, error);
    } else {
        setStatus(status);
    }
    if (Q_UNLIKELY(!QDBusConnection::sessionBus().send(reply))) {
        qCWarning(App) << "failed to send reply for" << status;
    }
    reply = QDBusMessage(); // reset the reply
}