// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __APPLICATION_H__
#define __APPLICATION_H__

#include <QVariantMap>
#include <QLoggingCategory>
#include <QtDBus/QDBusContext>
#include <QtDBus/QDBusMessage>

#include "interfaceHelper.h"

Q_DECLARE_LOGGING_CATEGORY(App)


class ApplicationAdaptor;


class Application: public ApplicationStatus, public QDBusContext {

    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.onyx.Application")

    Q_PROPERTY(QString package READ package CONSTANT)
    Q_PROPERTY(QVariantMap manifest READ manifest CONSTANT)
    Q_PROPERTY(qint64 instanceId READ instanceId CONSTANT)
    Q_PROPERTY(qint32 status READ status WRITE requestStatus NOTIFY statusChanged)

public:
    Application(const QVariantMap& manifest,
                const QString& package,
                QObject* parent = nullptr);

    virtual ~Application();

    virtual bool initialize() = 0;
    Q_SCRIPTABLE virtual void requestStatus(qint32 status);
    Q_SCRIPTABLE virtual bool enableInputs(bool enabled);
    Q_SCRIPTABLE virtual qint64 instanceId() const = 0;

    QString package() const noexcept;
    QVariantMap manifest() const noexcept;
    CurrentStatus status() const noexcept;

    QString objectPath() const noexcept;
    bool report(const QString& message, QtMsgType type = QtCriticalMsg);

signals:
    Q_SCRIPTABLE void statusChanged(qint32 status);

protected:
    Q_DISABLE_COPY_MOVE(Application)

    static bool grabInputsForProcess(qint64 pid);
    void responseReady( CurrentStatus status, bool success,
                        const QString& error=QString());

    bool isResponsePending(CurrentStatus status) const noexcept;
    bool setStatus(CurrentStatus status);
    virtual void processRequest(CurrentStatus status) = 0;

    CurrentStatus m_status;
    const QVariantMap m_manifest;
    const QString m_package;

private:
    QDBusMessage m_pendingReplies[CurrentStatus::Stopped + 1];
};

#endif // __APPLICATION_H__