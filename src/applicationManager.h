// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __APPLICATIONMANAGER_H__
#define __APPLICATIONMANAGER_H__

#include <QCoreApplication>
#include <QJsonObject>
#include <QHash>
#include <QMap>

#include <config.h>

#include "interfaceHelper.h"


class Config;
class Application;
class IRuntimeManager;
class ApplicationManagerAdaptor;
class QDir;
class QCommandLineParser;

// typedef QMap<QString, QMap<QString, QString>> ApplicationList;
typedef QVariantMap ApplicationList;


Q_DECLARE_METATYPE(ApplicationList)

#ifdef qApp
    #undef qApp
#endif
#define qApp (ApplicationManager::instance())


class ApplicationManager: public QCoreApplication {
    
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.onyx.ApplicationManager")

    Q_PROPERTY(QStringList applications READ applications)
    Q_PROPERTY(QStringList backingStores READ backingStores)
    Q_PROPERTY(QString installDirectory READ installDirectory)

public:
    ApplicationManager(int &argc, char **argv);
    ~ApplicationManager();

    static ApplicationManager* instance();

    QStringList applications() const;

    QString installDirectory() const noexcept;
    QStringList backingStores() const noexcept;
    Config* config() const noexcept;

    Q_SCRIPTABLE QString application(const QString& id) const;
    Q_SCRIPTABLE QString instance(qint64 id) const;
    Q_SCRIPTABLE bool setEnvironement(const QString& key, const QString& value);

    void setupCommand(const QCommandLineParser& parser);

    bool loadPlugin(const QString& pluginPath);

    Q_SCRIPTABLE void addBackingStore(const QString& path);
    static QStringList loadBackingStores();

    void addApplication(Application* application);
    void removeApplication(Application* application);

signals:
    Q_SCRIPTABLE void applicationAdded(const QString& path);
    Q_SCRIPTABLE void applicationError(const QString& id, const QString& msg);

private:
    void loadApplication(QVariantMap& manifest, const QString& id);
    struct RuntimeManagerEntry {
        QString name;
        QSharedPointer<IRuntimeManager> runtime;
    };
    typedef QList<RuntimeManagerEntry> RuntimeManagers;
    typedef QHash<QString, RuntimeManagers> RuntimeManagerByPlatform;
    typedef QHash<QString, Application*> Applications;
    typedef QHash<qint64, Application*> RunningApplications;

    RuntimeManagerByPlatform m_runtimes;
    Applications m_applications;
    RunningApplications m_runningApplications;
    QStringList m_backingStore;
    QString m_installDir;

    Config* m_config = nullptr;
    ApplicationManagerAdaptor* m_dbus_adaptor;
};


#endif // __APPLICATIONMANAGER_H__