// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __NATIVEAPPLICATION_H__
#define __NATIVEAPPLICATION_H__

#include <sstream>
#include "application.h"

class QProcess;
class QTimer;
class QProcessEnvironment;
class NativeRuntime;


class NativeApplication: public Application {

    Q_OBJECT
public:
    NativeApplication(  const QVariantMap& manifest,
                        const QString& package,
                        NativeRuntime* parent = nullptr);
    virtual ~NativeApplication();

    virtual qint64 instanceId() const override;
    virtual bool initialize() override;

    virtual void processRequest(CurrentStatus status) override;

    void setRunning(CurrentStatus status);
    void setPaused(CurrentStatus status);
    void setStopped(CurrentStatus status);

    static QStringList parseArguments(const QString& args);

    NativeRuntime* runtime() const noexcept;

private:
    void setupSignalHandlers(bool reportOutputs);
    void readStream(std::stringstream& stream,
                    const QString& data, QtMsgType type);

protected:
    virtual QProcessEnvironment setupEnvironment();

    std::stringstream m_stdout, m_stderr;
    QTimer* m_timer;
    QProcess* m_process;
};

#endif // __NATIVEAPPLICATION_H__