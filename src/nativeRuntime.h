// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __NATIVERUNTIME_H__
#define __NATIVERUNTIME_H__

#include "runtimeManager.h"

class NativeApplication;

class NativeRuntime: public QObject, public IRuntimeManager {

    Q_OBJECT
    Q_INTERFACES(IRuntimeManager)

public:
    NativeRuntime(QObject* parent = nullptr);
    virtual ~NativeRuntime() = default;

    virtual Application* createApplication( const QVariantMap& manifest,
                                            const QString& id) override;
    
    virtual bool initialize() override;

    QMap<QString, QString> getEnvironment() const;

protected:
    typedef QMap<QString, QString> EnvironmentVariables;
    typedef QHash<QString, NativeApplication*> Applications;
    Applications m_applications;
    EnvironmentVariables m_environmentVariables;
};

#endif // __NATIVERUNTIME_H__