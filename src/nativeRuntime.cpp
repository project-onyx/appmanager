// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <config.h>

#include "applicationManager.h"
#include "nativeApplication.h"
#include "nativeRuntime.h"


Application* NativeRuntime::createApplication(  const QVariantMap& manifest,
                                                const QString& id) {
    
    auto* application = new NativeApplication(manifest, id, this);
    return *m_applications.insert(id, application);
}


bool NativeRuntime::initialize() {

    Config* config = ApplicationManager::instance()->config();

    QStringList keys = config->keys("native_runtime.environment/*");
    for (const QString& key : keys) {
        QString name = key.mid(key.lastIndexOf('/') + 1);
        m_environmentVariables.insert(name, config->get<QString>(key));
    }
    return true;
}

QMap<QString, QString> NativeRuntime::getEnvironment() const {
    return m_environmentVariables;
}


NativeRuntime::NativeRuntime(QObject* parent): QObject(parent) {

    
}
