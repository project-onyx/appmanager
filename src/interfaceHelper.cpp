// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// #include <QDebug>
// #include <QtDBus/qdbusmetatype.h>
// #include <utils.h>
// #include <QDataStream>

#include "interfaceHelper.h"



// inline const QDBusArgument& operator>>( const QDBusArgument& argument,
//                                         CurrentStatus& status) {

//     int statusInt;
//     argument.beginStructure();
//     argument >> statusInt;
//     argument.endStructure();

//     if (Q_UNLIKELY(statusInt < 0 || statusInt > CurrentStatus::Stopped)) {
//         status = CurrentStatus::Unknown;
//     } else {
//         status = static_cast<CurrentStatus>(statusInt);
//     }
//     return argument;
// }


// inline QDBusArgument& operator<<(   QDBusArgument& argument,
//                                     const CurrentStatus& status) {

//     argument.beginStructure();
//     argument << int(status);
//     argument.endStructure();

//     return argument;
// }

// inline QDataStream& operator<<(QDataStream& stream, const CurrentStatus& status) {
//     stream << quint8(status);
//     return stream;
// }

// void register_types() {
//     do_register_types();
// }

// inline QDataStream& operator>>(QDataStream& stream, CurrentStatus& status) {
//     quint8 statusInt;
//     stream >> statusInt;
//     if (Q_UNLIKELY(statusInt > CurrentStatus::Stopped)) {
//         status = CurrentStatus::Unknown;
//     } else {
//         status = static_cast<CurrentStatus>(statusInt);
//     }
//     return stream;
// }

// void initInterfaceHelper() {
//     qDebug() << "Initializing interface helper";
//     qRegisterMetaType<CurrentStatus>();
//     qRegisterMetaTypeStreamOperators<CurrentStatus>();

//     qDBusRegisterMetaType<DBusVariantMap>();
//     qDBusRegisterMetaType<QDBusObjectPathMap>();
//     qDBusRegisterMetaType<CurrentStatus>();
// }
