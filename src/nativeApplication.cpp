// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QProcess>
#include <QTimer>
#include <csignal>

#include <iostream>
#include <config.h>

#include "nativeRuntime.h"
#include "applicationManager.h"
#include "nativeApplication.h"


Q_LOGGING_CATEGORY(nativeApp, "driver.usb")



CurrentStatus mapStatus(QProcess::ProcessState state) {

    switch (state) {
        case QProcess::NotRunning:
        case QProcess::Starting: return CurrentStatus::Stopped;
        case QProcess::Running: return CurrentStatus::Running;
    }
    Q_UNREACHABLE();
    return CurrentStatus::Unknown;
}


NativeApplication::NativeApplication(   const QVariantMap& manifest,
                                        const QString& package,
                                        NativeRuntime* parent):
    Application(manifest, package, parent),
    m_process(new QProcess(this)),
    m_timer(new QTimer(this)) {

    Config* config = ApplicationManager::instance()->config();

    int killTimeout = config->get<int>("native_application/kill_timeout",
                                        DEFAULT_PROCESS_KILL_TIMEROUT_MSECS);

    bool reportOutputs = config->get<bool>("native_application/report_outputs",
                                            DEFAULT_PROCESS_REPORT_OUTPUTS);

    setupSignalHandlers(reportOutputs);

    m_timer->setSingleShot(true);
    m_timer->setInterval(killTimeout);
}


QStringList NativeApplication::parseArguments(const QString& arguments) {

    QStringList args;
    QString arg;
    bool inQuote = false;
    for (int i = 0; i < arguments.size(); ++i) {
        const QChar& c = arguments.at(i);
        if (c == '"') {
            inQuote = !inQuote;
        } else if (c == ' ' && !inQuote) {
            args << arg;
            arg.clear();
        } else {
            arg += c;
        }
    }
    if (!arg.isEmpty()) {
        args << arg;
    }
    return args;
}


NativeRuntime* NativeApplication::runtime() const noexcept {
    return static_cast<NativeRuntime*>(parent());    
}


bool NativeApplication::initialize() {

    QString command = m_manifest["Desktop Entry/Exec"].toString();
    QStringList commandParts = parseArguments(command);

    if (!commandParts.isEmpty()) {
        m_process->setProgram(commandParts.takeFirst());
        m_process->setArguments(commandParts);
        return true;
    }
    return false;
}


void NativeApplication::processRequest(CurrentStatus status) {

    switch (status) {
        case CurrentStatus::Running: return setRunning(status);
        case CurrentStatus::Stopped: return setStopped(status);
        case CurrentStatus::Paused: return setPaused(status);
    }
    Q_UNREACHABLE();
}


NativeApplication::~NativeApplication() {
    
    if (m_process->state() != QProcess::NotRunning) {
        if (!m_process->waitForFinished(m_timer->interval())) {
            m_process->kill();
        }
        Application::setStatus(CurrentStatus::Stopped);
    }
}


qint64 NativeApplication::instanceId() const {
    return m_process->processId();
}


QProcessEnvironment NativeApplication::setupEnvironment() {
    
    QProcessEnvironment environ = QProcessEnvironment::systemEnvironment();
    QMap<QString, QString> configEnv = runtime()->getEnvironment();

    for (auto it = configEnv.begin(); it != configEnv.end(); ++it) {
        environ.insert(it.key(), it.value());
    }
    const QRegularExpression expr("\\$\\{([^}]+)\\}");

    for (const QString& key: environ.keys()) {
        QString parsedValue = environ.value(key);
        int pos = 0;
        QRegularExpressionMatch match;

        while ((match = expr.match(parsedValue, pos)).hasMatch()) {
            QString name = match.captured(1);

            if (m_manifest.contains(name)) {
                QString value = m_manifest[name].toString();
                parsedValue.replace(match.capturedStart(), match.capturedEnd(), value);
                pos += value.length();
            } else {
                pos = match.capturedEnd();
            }
        }
        environ.insert(key, parsedValue);
    }
    return environ;
}


void NativeApplication::setRunning(CurrentStatus status) {

    if (m_status != CurrentStatus::Paused) {
        m_process->setProcessEnvironment(setupEnvironment());
        m_process->start();
    } else if (kill(m_process->processId(), SIGCONT) != 0) {
        responseReady(status, false, tr("Failed to resume"));
    } else {
        responseReady(status, true);
    }
}


void NativeApplication::setPaused(CurrentStatus status) {
    
    if (m_status != CurrentStatus::Running) {
        responseReady(status, false, tr("Not running"));
    } else if (kill(m_process->processId(), SIGSTOP) != 0) {
        responseReady(status, false, tr("Failed to pause"));
    } else {
        responseReady(status, true);
    }
}


void NativeApplication::setStopped(CurrentStatus status) {

    if (m_status != CurrentStatus::Paused) {
        // If the process is not paused, we can kill it.
    } else if (kill(m_process->processId(), SIGCONT) != 0) {
        return responseReady(status, false, tr("Failed to resume process"));
    }
    m_timer->start();
    m_process->terminate();
}


void NativeApplication::setupSignalHandlers(bool reportOutputs) {
    
    auto lgEnd = QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished);

    using ProcessState = QProcess::ProcessState;
    using ProcessError = QProcess::ProcessError;

    connect(m_process, lgEnd, m_timer, &QTimer::stop);
    connect(m_timer, &QTimer::timeout, m_process, &QProcess::kill);

    connect(m_process, &QProcess::errorOccurred, [this](ProcessError error) {
        QString errorString = m_process->errorString();

        if (error != QProcess::FailedToStart) {
            report(errorString);
        } else {
            responseReady(CurrentStatus::Running, false, errorString);
        }
    });
    connect(m_process, &QProcess::stateChanged, [this](ProcessState state) {
        CurrentStatus status = mapStatus(state);
        if (m_status != status) {
            responseReady(status, true);
        }
    });
    if (reportOutputs) {
        connect(m_process, &QProcess::readyReadStandardOutput, [this] {
            std::cout << m_process->readAllStandardOutput().toStdString();
            // readStream(m_stdout, m_process->readAllStandardOutput(), QtInfoMsg);
        });
        connect(m_process, &QProcess::readyReadStandardError, [this] {
            std::cout << m_process->readAllStandardError().toStdString();
            // readStream(m_stderr, m_process->readAllStandardError(), QtWarningMsg);
        });
    }
}

void NativeApplication::readStream( std::stringstream& stream,
                                    const QString& data, QtMsgType type) {
    if (data.endsWith('\n')) {
        report(QString::fromStdString(stream.str()), type);
        stream.clear();
    } else {
        stream << data.toStdString();
    }
}
