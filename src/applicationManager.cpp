// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QDir>
#include <QJsonArray>
#include <QPluginLoader>
#include <QSettings>

#include <environmentVariableProvider.h>
#include <common_utils.h>

#include "applicationmanageradaptor.h"

#include "application.h"
#include "app_config.h"
#include "config.h"
#include "managerinterface.h"
#include "mountinterface.h"
#include "nativeRuntime.h"
#include "applicationManager.h"



#define INTERFACE_NAME PACKAGE_BASE ".ApplicationManager"



static QVariantMap readManifest(const QString& path) {

    QSettings settings(path, QSettings::IniFormat);

    QVariantMap manifest;
    for (const QString& key : settings.allKeys()) {
        manifest.insert(key, settings.value(key));
    }

    return manifest;
}


static inline QString preferredRuntime(const QString& platform) {

    QString key("platform:%1/runtime");
    return qApp->config()->value(key.arg(platform)).toString();
}


QString getMountOptions(const QString& name, const QString& options) {

    for (const QString& option : options.split(QLatin1Char(','))) {
        if (option == name) {
            return QString("1");
        } else if (!option.startsWith(name + "=")) {
            return option.mid(name.length() + 1);
        }
    }
    return QString("0");
}

QString setMountOptions(const QString& old, const QString& name, const QString& value) {

    QStringList options = old.split(QLatin1Char(','));

    for (int i = 0; i < options.size(); ++i) {
        if (options.startsWith(name + "=")) {
            options[i] = name + "=" + value;
            return options.join(QLatin1Char(','));
        }
    }
    return old + QLatin1Char(',') + name + "=" + value;
}

Q_OS_LINUX

ApplicationManager::ApplicationManager(int &argc, char **argv):
    QCoreApplication(argc, argv),
     m_dbus_adaptor(new ApplicationManagerAdaptor(this)),
     m_installDir(DEFAULT_INSTALL_DIR),
     m_backingStore(loadBackingStores())
{
    // setup the dbus interface
    QDBusConnection::sessionBus().registerService(INTERFACE_NAME);
    QDBusConnection::sessionBus().registerObject(
        "/ApplicationManager",
        m_dbus_adaptor,
        QDBusConnection::ExportAllContents
    );
    auto& runtimes = m_runtimes[QSysInfo::kernelType()];

    runtimes.append(RuntimeManagerEntry{
        .name = "NativeRuntime",
        .runtime = QSharedPointer<IRuntimeManager>(new NativeRuntime)
    });
}

ApplicationManager::~ApplicationManager() {

    QDBusConnection::sessionBus().unregisterService(INTERFACE_NAME);
    QDBusConnection::sessionBus().unregisterObject("/ApplicationManager");
}


ApplicationManager* ApplicationManager::instance() {
    return static_cast<ApplicationManager*>(QCoreApplication::instance());
}


QStringList ApplicationManager::applications() const {
    QStringList result;

    for (auto it = m_applications.cbegin(); it != m_applications.cend(); ++it) {
        result.emplace_back(it.value()->objectPath());
    }
    return result;
}

QString ApplicationManager::installDirectory() const noexcept {
    return m_installDir;
}

QStringList ApplicationManager::backingStores() const noexcept {
    return m_backingStore;
}

Config* ApplicationManager::config() const noexcept {
    return m_config;
}


QString ApplicationManager::instance(qint64 id) const {

    Application* application = m_runningApplications.value(id);

    if (Q_UNLIKELY(application == nullptr)) {
        return QString();
    }
    return application->package();
}

bool ApplicationManager::setEnvironement(const QString& key, const QString& value) {
    return qputenv(key.toUtf8().constData(), value.toUtf8().constData());
}


QString ApplicationManager::application(const QString& id) const {
    
    Application* application = m_applications.value(id);

    if (Q_UNLIKELY(application == nullptr)) {
        return QString();
    }
    return application->objectPath();
}


void ApplicationManager::setupCommand(const QCommandLineParser& parser) {
    Q_ASSERT(m_config == nullptr);

    m_config = new Config(parser.value("config"), Config::NativeFormat, this);

    m_config->registerProvider(
        new EnvironmentVariableProvider("env", this)
    );

    QStringList dirs = m_config->get<QStringList>("core/plugins",
                                            DEFAULT_PLUGIN_DIR);

    QString appDir = m_config->get<QString>("core/app_dir",
                                            DEFAULT_APP_DIR);

    for (const QDir& pluginsDir : dirs) {
        for (const QString& pluginPath : pluginsDir.entryList(QDir::Files)) {
            const QString absPath = pluginsDir.absoluteFilePath(pluginPath);

            if (QLibrary::isLibrary(absPath)) {
                loadPlugin(absPath);
            }
        }
    }
    QDir dir(appDir);
    for (const QString& entry: dir.entryList({"*.desktop"}, QDir::Files)) {
        const QString absPath = dir.absoluteFilePath(entry);

        QVariantMap manifest = readManifest(absPath);
        loadApplication(manifest, entry.left(entry.length() - 8));
    }
}


bool ApplicationManager::loadPlugin(const QString& pluginPath) {

    QPluginLoader loader(pluginPath);

    if (!loader.load()) {
        qWarning() << "Failed to load plugin" << pluginPath << loader.errorString();
        return false;
    }
    QObject* plugin = loader.instance();
    QSharedPointer<IRuntimeManager> runtime;

    if (IRuntimeManager* casted = qobject_cast<IRuntimeManager*>(plugin)) {
        runtime.reset(casted);
    } else {
        qWarning() << "Plugin do not implements IRuntimeManager";
        plugin->deleteLater();
        return false;
    }
    const QJsonObject metaData = loader.metaData().value("MetaData").toObject();

    QString name = metaData.value("Name").toString();
    QJsonArray platforms = metaData.value("Platforms").toArray();

    for (const QJsonValue& platformValue : platforms) {
        QString platform = platformValue.toString();

        QString preferred = preferredRuntime(platform);
        RuntimeManagers& managers = m_runtimes[platform];

        managers.insert(
            preferred == name ? managers.begin() : managers.end(),
            RuntimeManagerEntry {
                .name = name, 
                .runtime = runtime
        });
    }
    return runtime->initialize();
}



void ApplicationManager::loadApplication(QVariantMap& manifest, const QString& id) {
    
    QString platform = manifest.value("Desktop Entry/X-Platform").toString();
    
    if (!manifest.contains("Desktop Entry/X-ApplicationID")) {
        manifest.insert("Desktop Entry/X-ApplicationID", id);
    }
    if (!platform.isEmpty() && m_runtimes.contains(platform)) {
        Application* app = nullptr;
        RuntimeManagers& managers = m_runtimes[platform];
        auto it=managers.cbegin();

        while (it != managers.cend() && !app) {
            app = it->runtime->createApplication(manifest, id);
            ++it;
        }
        if (Q_LIKELY(app && app->initialize())) {
            addApplication(app);
        } else {
            qWarning() << "Failed to load application" << id;
        }
    }
}


void ApplicationManager::addBackingStore(const QString& path) {
    
    if (Q_UNLIKELY(m_backingStore.contains(path))) {
        qWarning() << "Backing store"<< path << "already exists";
        return;
    }
    OrgFreedesktopSystemd1ManagerInterface manager("org.freedesktop.systemd1",
                                                   "/org/freedesktop/systemd1",
                                                   QDBusConnection::systemBus());

    QDBusReply<QDBusObjectPath> unit = manager.GetUnit("application.mount");

    if (Q_UNLIKELY(!unit.isValid())) {
        qWarning() << "Failed to find application.mount unit";
        return;
    }
    // For secuity, we check if option separator is not injected in the path.
    if (Q_UNLIKELY(path.contains(QLatin1Char(',')))) {
        qWarning() << "Invalid path (forbidden characted ','): " << path;
        return;
    }
    OrgFreedesktopSystemd1MountInterface mount("org.freedesktop.systemd1",
                                               unit.value().path(),
                                               QDBusConnection::systemBus());

    m_backingStore.append(path);
    QString opts = setMountOptions( mount.options(), "upperdir",
                                    m_backingStore.join(":"));

    manager.SetUnitProperties(  "application.mount", false,
                                QVariantMap({{"options", opts}}));
}




QStringList ApplicationManager::loadBackingStores() {
    return {};
    OrgFreedesktopSystemd1ManagerInterface manager("org.freedesktop.systemd1",
                                                   "/org/freedesktop/systemd1",
                                                   QDBusConnection::systemBus());

    QDBusReply<QDBusObjectPath> unit = manager.GetUnit("application.mount");

    if (Q_UNLIKELY(!unit.isValid())) {
        qWarning() << "Failed to find application.mount unit";
        return QStringList();
    }
    OrgFreedesktopSystemd1MountInterface mount("org.freedesktop.systemd1",
                                               unit.value().path(),
                                               QDBusConnection::systemBus());

    QString layersStr = getMountOptions("lowerdir", mount.options());
    return layersStr.split(":");
}


void ApplicationManager::addApplication(Application* app) {

    QString id = app->package();

    if (Q_UNLIKELY(m_applications.contains(id))) {
        qWarning() << "Application" << id << "already exists";
        return;
    }
    m_applications.insert(app->package(), app);

    connect(app, &Application::destroyed, [](QObject* obj) {
        qApp->removeApplication(qobject_cast<Application*>(obj));
    });

    connect(app, &Application::statusChanged, [this, app] {
        Application::CurrentStatus status = app->status();

        if (status == Application::CurrentStatus::Running) {
            qint64 instanceId = app->instanceId();
            m_runningApplications.insert(instanceId, app);
        } else if (status == Application::CurrentStatus::Stopped) {
            m_runningApplications.remove(app->instanceId());
        }
    });
    emit applicationAdded(app->objectPath());
}


void ApplicationManager::removeApplication(Application* application) {

    QString id = application->package();

    if (Q_UNLIKELY(!m_applications.contains(id))) {
        qWarning() << "Application" << id << "does not exist";
        return;
    }
    m_applications.remove(id);
}